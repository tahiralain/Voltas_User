package net.voltasinc.voltasuser;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;

public class FCMInitializationService extends FirebaseInstanceIdService
{
    @Override
    public void onTokenRefresh()
    {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        CommonObjects.saveSharedPreferences(getApplicationContext(), "fcmId", fcmToken);
    }
}
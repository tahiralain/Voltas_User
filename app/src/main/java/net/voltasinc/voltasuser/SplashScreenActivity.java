package net.voltasinc.voltasuser;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;

public class SplashScreenActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Hide the status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Hiding action bar
        getSupportActionBar().hide();

        // animation of logo
        startAnimations();
    }// <-- End Of onCreate() Method




    // --> Start Apply animation on Logo
    private void startAnimations()
    {

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.translate);
        animation.reset();
        ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);
        ivLogo.clearAnimation();
        ivLogo.startAnimation(animation);
        // When animation end enable the buttons
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }
            @Override
            public void onAnimationEnd(Animation animation)
            {
                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Intent i = null;

                        if((Boolean) CommonObjects.getSharedPreferences(SplashScreenActivity.this, "LoggedIn", false))
                        {
                            i= new Intent(SplashScreenActivity.this,AfterLoginActivity.class);
                        }
                        else
                        {
                            i=  new Intent(SplashScreenActivity.this,SelectLoginActivity.class);
                        }

                        startActivity(i);
                        SplashScreenActivity.this.finish();
                    }
                },2000);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
    }
    // <-- End of StartAnimations() method

}

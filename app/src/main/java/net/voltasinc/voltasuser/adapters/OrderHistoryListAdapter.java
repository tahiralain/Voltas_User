package net.voltasinc.voltasuser.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.voltasinc.voltasuser.OrderCarConfirmMapActivity;
import net.voltasinc.voltasuser.R;
import net.voltasinc.voltasuser.model.OrderCarHistoryDataItems;

import java.util.ArrayList;

/**
 * Created by hp on 1/31/2017.
 */
public class OrderHistoryListAdapter extends RecyclerView.Adapter<OrderHistoryListAdapter.MyViewHolder>
{
    private ArrayList<OrderCarHistoryDataItems> values;
    Context context;
    public static OrderCarHistoryDataItems orderHistoryData;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvOrderId,tvPickUpLocation,tvPickUpDate,tvInfrontOf,driverID;
        private ImageView ivGo;

        private LinearLayout liMain;
        public MyViewHolder(View view)
        {
            super(view);
            tvOrderId = (TextView) view.findViewById(R.id.tvOrderId);
            tvPickUpLocation = (TextView) view.findViewById(R.id.tvPickUpLocation);
            tvPickUpDate = (TextView) view.findViewById(R.id.tvPickUpDate);
            tvInfrontOf = (TextView) view.findViewById(R.id.tvInfrontOf);
            ivGo = (ImageView) view.findViewById(R.id.ivGo);
            liMain = (LinearLayout) view.findViewById(R.id.liMain);
            driverID = (TextView) view.findViewById(R.id.driverID);
        }
    }

    public OrderHistoryListAdapter(ArrayList<OrderCarHistoryDataItems> values, Context context)
    {
        this.values = values;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_order_history_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final OrderCarHistoryDataItems orderCarHistoryDataItems = values.get(position);

        holder.tvOrderId.setText(orderCarHistoryDataItems.getOrderId());

        holder.tvPickUpLocation.setText(orderCarHistoryDataItems.getOrderLocation());
        holder.tvInfrontOf.setText(orderCarHistoryDataItems.getOrderInfrontOf());
        holder.tvPickUpDate.setText(orderCarHistoryDataItems.getOrderDate()+" "+orderCarHistoryDataItems.getOrderTime());
        holder.driverID.setText(orderCarHistoryDataItems.getDriverId());

        holder.tvOrderId.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                orderHistoryData = orderCarHistoryDataItems;

                Intent intent = new Intent(context,OrderCarConfirmMapActivity.class);
                intent.putExtra("driverID", holder.driverID.getText());
                Toast.makeText(context, holder.driverID.getText(), Toast.LENGTH_SHORT).show();
                context.startActivity(intent);
            }
        });

        holder.liMain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                orderHistoryData = orderCarHistoryDataItems;

                Intent intent = new Intent(context,OrderCarConfirmMapActivity.class);
                intent.putExtra("driverID", holder.driverID.getText());
                Toast.makeText(context, holder.driverID.getText(), Toast.LENGTH_SHORT).show();
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return values.size();
    }
}

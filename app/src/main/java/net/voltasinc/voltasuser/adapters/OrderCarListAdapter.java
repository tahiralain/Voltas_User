package net.voltasinc.voltasuser.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.OrderCarDetailsActivity;
import net.voltasinc.voltasuser.OrderCarListActivity;
import net.voltasinc.voltasuser.R;
import net.voltasinc.voltasuser.ReserveCarDetailsActivity;
import net.voltasinc.voltasuser.model.carCategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 1/31/2017.
 */
public class OrderCarListAdapter extends RecyclerView.Adapter<OrderCarListAdapter.MyViewHolder>
{
    private ArrayList<String> values;
    Context context;

    List<carCategoryModel> CarCategoryData;

    private  int[] carImages =
            { R.drawable.bmw, R.drawable.ferrari, R.drawable.suv, R.drawable.limousine };

    private  String[] carNames = {"Voltas Black", "Voltas XL", "Voltas Select", "Voltas VIP"};

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvCarName,tvCarPrice;
        ImageView ivOrderCarImage;

        private LinearLayout liMain;
        public MyViewHolder(View view)
        {
            super(view);
            tvCarName = (TextView) view.findViewById(R.id.tvCarName);
            tvCarPrice = (TextView) view.findViewById(R.id.tvCarPrice);
            ivOrderCarImage = (ImageView) view.findViewById(R.id.ivOrderCarImage);
            liMain = (LinearLayout) view.findViewById(R.id.liMain);
        }
    }

    public OrderCarListAdapter(List<carCategoryModel> CarCategoryData, Context context)
    {
        this.CarCategoryData = CarCategoryData;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_order_car_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final carCategoryModel getDataAdapter1 =  CarCategoryData.get(position);

        holder.tvCarName.setText(getDataAdapter1.getCategoryName());
        holder.ivOrderCarImage.setImageResource(carImages[position]);
        holder.tvCarPrice.setText(getDataAdapter1.getCategoryPerKMFare());

        holder.tvCarName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonObjects.saveSharedPreferences(context, "SelectedCarID",getDataAdapter1.getCategoryID());
                CommonObjects.saveSharedPreferences(context, "SelectedCarName",getDataAdapter1.getCategoryName());
                CommonObjects.saveSharedPreferences(context, "SelectedCarPrice",getDataAdapter1.getCategoryPerKMFare());
               context.startActivity(new Intent(context, OrderCarDetailsActivity.class));
            }
        });
        holder.liMain.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonObjects.saveSharedPreferences(context, "SelectedCarID",getDataAdapter1.getCategoryID());
                CommonObjects.saveSharedPreferences(context, "SelectedCarName",getDataAdapter1.getCategoryName());
                CommonObjects.saveSharedPreferences(context, "SelectedCarPrice",getDataAdapter1.getCategoryPerKMFare());

                //Toast.makeText(context, String.valueOf(CommonObjects.getSharedPreferences(context, "SelectedCarID", "")), Toast.LENGTH_SHORT).show();

                context.startActivity(new Intent(context, OrderCarDetailsActivity.class));
            }
        });
        holder.ivOrderCarImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonObjects.saveSharedPreferences(context, "SelectedCarID",getDataAdapter1.getCategoryID());
                CommonObjects.saveSharedPreferences(context, "SelectedCarName",getDataAdapter1.getCategoryName());
                CommonObjects.saveSharedPreferences(context, "SelectedCarPrice",getDataAdapter1.getCategoryPerKMFare());

                //Toast.makeText(context, String.valueOf(CommonObjects.getSharedPreferences(context, "SelectedCarID", "")), Toast.LENGTH_SHORT).show();

                context.startActivity(new Intent(context, OrderCarDetailsActivity.class));
            }
        });
        holder.tvCarPrice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CommonObjects.saveSharedPreferences(context, "SelectedCarID",getDataAdapter1.getCategoryID());
                CommonObjects.saveSharedPreferences(context, "SelectedCarName",getDataAdapter1.getCategoryName());
                CommonObjects.saveSharedPreferences(context, "SelectedCarPrice",getDataAdapter1.getCategoryPerKMFare());
                context.startActivity(new Intent(context, OrderCarDetailsActivity.class));
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return CarCategoryData.size();
    }
}

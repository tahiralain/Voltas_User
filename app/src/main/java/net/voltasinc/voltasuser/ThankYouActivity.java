package net.voltasinc.voltasuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.TextView;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThankYouActivity extends AppCompatActivity {

    @BindView(R.id.tvOrderCar)
    TextView tvOrderCar;
    @BindView(R.id.tvOrderDate)
    TextView tvOrderDate;
    @BindView(R.id.tvPickUp)
    TextView tvPickUp;
    @BindView(R.id.tvDone)
    TextView tvDone;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        ButterKnife.bind(this);

        changeActionBarText("Thank You");

        Bundle bundle = getIntent().getExtras();

        if(bundle != null)
        {
            tvOrderDate.setText(bundle.getString("date")+" "+bundle.getString("time"));
            tvPickUp.setText(bundle.getString("location"));

            if(!CommonObjects.getSharedPreferences(this, "SelectedCarName","").toString().equals(""))
            {
               String selectedCarName = CommonObjects.getSharedPreferences(this, "SelectedCarName","").toString();
               String selectedCarPrice = CommonObjects.getSharedPreferences(this, "SelectedCarPrice","").toString();
               tvOrderCar.setText(selectedCarName+" "+selectedCarPrice);
            }
        }
    }


    public void changeActionBarText(String text)
    {
        ActionBar a = getSupportActionBar();
        a.setCustomView(R.layout.layout_item_actionbar);
        a.setDisplayShowCustomEnabled(true);
        TextView tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        tvActionBarTitle.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transparent, menu);
        return true;
    }

    @OnClick(R.id.tvDone)
    public void onClick()
    {
        Intent intent = new Intent(this, AfterLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        this.finish();
    }
}

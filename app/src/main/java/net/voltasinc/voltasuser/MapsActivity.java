package net.voltasinc.voltasuser;

import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.dialogs.CustomAlertDialog;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.ivTick)
    ImageView ivTick;
    @BindView(R.id.tvSearchAddress)
    TextView tvSearchAddress;
    private GoogleMap mMap;
    String address = "";

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;

    private static final long INTERVAL = 10000; //10 Secs
    private static final long FASTEST_INTERVAL = 10000; // 10 Secs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(15);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        new CustomAlertDialog(this, "Pick Your Address", "First Long Press The Marker Then Drag It To Your PickUp Location");
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        Double latitude = Double.parseDouble((String) CommonObjects.getSharedPreferences(this, "userLatitude", "0"));
        Double longitude = Double.parseDouble((String) CommonObjects.getSharedPreferences(this, "userLongitude", "0"));

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(sydney).title("Marker in Sydney");
        markerOptions.draggable(true);
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

        mMap.setOnMarkerDragListener(this);

        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {

            addresses = gcd.getFromLocation(latitude, longitude, 1);
            CommonObjects.saveSharedPreferences(this, "orderCarLongitude",String.valueOf(latitude));
            CommonObjects.saveSharedPreferences(this, "orderCarLatitude",String.valueOf(longitude));

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        if (addresses.size() > 0)
        {
            Address add = addresses.get(0);
            address = add.getLocality() + " " + add.getAddressLine(0);
        }

    }


    @Override
    public void onBackPressed()
    {
        addressPicked();
    }

    public void addressPicked()
    {
        getIntent().putExtra("result", address);
        setResult(RESULT_OK, getIntent());

        finish();
    }


    @Override
    public void onMarkerDragStart(Marker marker)
    {
        //Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker)
    {

    }

    @Override
    public void onMarkerDragEnd(Marker marker)
    {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            LatLng ll = marker.getPosition();

            addresses = gcd.getFromLocation(ll.latitude, ll.longitude, 1);
            CommonObjects.saveSharedPreferences(this, "orderCarLongitude", String.valueOf(ll.latitude));
            CommonObjects.saveSharedPreferences(this, "orderCarLatitude", String.valueOf(ll.longitude));

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        if (addresses.size() > 0)

        {
            Address add = addresses.get(0);
            marker.setTitle(add.getLocality());
            marker.setSnippet(add.getAddressLine(0));

            address = add.getLocality() + " " + add.getAddressLine(0);
            marker.showInfoWindow();
        }
    }

    @OnClick(R.id.ivTick)
    public void onClick()
    {
        addressPicked();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        LatLng currLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        Log.e("location received", location.toString());
    }
}

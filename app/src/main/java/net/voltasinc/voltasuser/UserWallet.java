package net.voltasinc.voltasuser;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.CustomClasses.paypalConfig;
import net.voltasinc.voltasuser.adapters.OrderHistoryListAdapter;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserWallet extends AppCompatActivity {

    @BindView(R.id.userWalletBalance)
    TextView userWalletBalance;
    @BindView(R.id.walletTopup)
    Button walletTopup;

    private ProgressDialog progressDialog;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    public static double walletTopupAmountValue = 0.0d;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;

    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(paypalConfig.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_wallet);
        ButterKnife.bind(this);

        String userWallet = (String) CommonObjects.getSharedPreferences(getApplicationContext(), "userWallet", "");

        getWalletBalance();

        walletTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showWalletTopupAmountDialog();
            }
        });
    }

    public void showWalletTopupAmountDialog()
    {
        final Dialog dialog = new Dialog(UserWallet.this);
        dialog.setContentView(R.layout.layout_wallet_topup_amount);
        dialog.setTitle("Wallet Topup");

        final EditText walletTopupAmount = (EditText) dialog.findViewById(R.id.walletTopupAmount);

        Button topupConfirm = (Button) dialog.findViewById(R.id.walletTopupButtonDialog);

        topupConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                walletTopupAmountValue = Double.valueOf(walletTopupAmount.getText().toString());
                walletTopup(walletTopupAmount.getText().toString());
            }
        });
        dialog.show();
    }

    protected void walletTopup(String topupAmount)
    {
        //Creating a paypalpayment

        PayPalPayment payment = new PayPalPayment(new BigDecimal(topupAmount), "USD", "Wallet Topup Payment",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        //Starting a new activity for the payment details and also putting the payment details with intent
                        JSONObject jsonDetails = new JSONObject(paymentDetails);
                        JSONObject jsonDetails2 = jsonDetails.getJSONObject("response");

                        final String paymentID = jsonDetails2.getString("id");
                        String status = jsonDetails2.getString("state");

                        String message = "Your Wallet Topup Request Has Been " + status + ". Your Payment ID is: " + paymentID + ". Your Payment Amount Was: $" + walletTopupAmountValue;

                        if(status.equalsIgnoreCase("approved")) {
                            new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Congratulations!")
                                    .setContentText(message)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            sendPaymentDetailsToServer(paymentID, String.valueOf(walletTopupAmountValue));
                                        }
                                    })
                                    .show();
                        } else {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Sorry!")
                                    .setContentText("Your Payment Wasn't Approved. Please Contact Your Administrator.")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        }

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    protected void getWalletBalance()
    {
        final String[] WalletBalance = {""};

        progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",UserWallet.this);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        HashMap<String, String> body = new HashMap<>();
        body.put("userId", CommonObjects.getSharedPreferences(this, "userId", "").toString());
        Log.e("JSON Body", body.toString());

        service.viewUserWalletBalance(body).enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                progressDialog.dismiss();

                if (!json.equals("") || json != null || !json.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        Log.e("Wallet View Response", jsonObject.toString());

                        userWalletBalance.setText("$" + jsonObject.getString("WalletBalance"));

                    } catch (JSONException e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();

                    }
                } else
                {
                    progressDialog.dismiss();
                    Toast.makeText(UserWallet.this, "Internet Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    protected void sendPaymentDetailsToServer(String PaymentID, String Amount){

        progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",UserWallet.this);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        HashMap<String, String> body = new HashMap<>();
        body.put("userId", CommonObjects.getSharedPreferences(this, "userId", "").toString());
        body.put("newWalletAmount", Amount);
        body.put("paymentID", PaymentID);
        body.put("transactionType", "Payment Added To Wallet");

        Log.e("JSON Body", body.toString());

        service.updateUserWalletBalance(body).enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                progressDialog.dismiss();


                if (!json.equals("") || json != null || !json.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        String success = jsonObject.getString("success");
                        String message = jsonObject.getString("message");

                        Log.e("Wallet Update Response", jsonObject.toString());

                        startActivity(new Intent(UserWallet.this, UserWallet.class));

                    } catch (JSONException e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();

                    }
                } else
                {
                    progressDialog.dismiss();
                    Toast.makeText(UserWallet.this, "Internet Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });


    }
}

package net.voltasinc.voltasuser;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReserveCarDetailsActivity extends AppCompatActivity {

    @BindView(R.id.txtTripType)
    EditText txtTripType;
    @BindView(R.id.txtPickUpAddress)
    EditText txtPickUpAddress;
    @BindView(R.id.txtDropOffAddress)
    EditText txtDropOffAddress;
    @BindView(R.id.txtPickUpDate)
    EditText txtPickUpDate;
    @BindView(R.id.txtDescription)
    EditText txtDescription;
    @BindView(R.id.tvGetPrice)
    TextView tvGetPrice;

    String tripType="One Way Trip";

    String paymentType = "Cash";

    String datePick,timePick;

    String orderPickupLatiutde, orderPickupLongitude, orderDropoffLatiutde, orderDropoffLongitude;

    String pickupPlaceName;
    String dropoffPlaceName;

    int PLACE_PICKUP_AUTOCOMPLETE_REQUEST_CODE = 1;
    int PLACE_DROPOFF_AUTOCOMPLETE_REQUEST_CODE = 2;

    ProgressDialog progressDialog;

    EditText paymentMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_car_details);
        ButterKnife.bind(this);

        txtPickUpAddress.setEnabled(false);
        txtPickUpAddress.setText(CommonObjects.getPickupAddress());

        paymentMethod = (EditText) findViewById(R.id.txtPaymentMethod);

        paymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentTypeDialog();
                //Toast.makeText(ReserveCarDetailsActivity.this, "Payment", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick({R.id.txtTripType, R.id.txtPickUpAddress, R.id.txtDropOffAddress, R.id.txtPickUpDate, R.id.tvGetPrice})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtTripType:
                dialogTripType();
                break;
            case R.id.txtPickUpAddress:
                break;
            case R.id.txtDropOffAddress:

                Intent nextintent =
                        null;
                try {
                    nextintent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(ReserveCarDetailsActivity.this);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                startActivityForResult(nextintent, PLACE_DROPOFF_AUTOCOMPLETE_REQUEST_CODE);

                break;
            case R.id.txtPickUpDate:
                dateTimeDialog();
                break;
            case R.id.txtPaymentMethod:
                break;
            case R.id.tvGetPrice:
                orderCar();
                break;
        }
    }

    private void dateTimeDialog()
    {
        // Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        date d = new date();
        time t = new time();
        TimePickerDialog tp = new TimePickerDialog(this,t,mHour, mMinute, false);
        tp.show();
        DatePickerDialog dpd = new DatePickerDialog(this,d, mYear, mMonth, mDay);
        dpd.show();
    }

    class  date implements DatePickerDialog.OnDateSetListener
    {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            datePick= " "+dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
        }
    }

    class  time implements TimePickerDialog.OnTimeSetListener
    {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute)
        {
            timePick=hourOfDay + ":" + minute;
            txtPickUpDate.setText(datePick+" "+timePick);
        }
    }

    private void paymentTypeDialog()
    {
        final String[] selectedPaymentType = new String[1];

        View view = getLayoutInflater().inflate(R.layout.dialog_payment_type,null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        TextView tvSelect = (TextView) view.findViewById(R.id.tvSelect);
        Spinner spinner = (Spinner) view.findViewById(R.id.txtCardType);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                selectedPaymentType[0] = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Cash");
        categories.add("Credit Card");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter <String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);


        final AlertDialog alert = builder.create();
        alert.show();

        tvSelect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                paymentMethod.setText(selectedPaymentType[0]);
                alert.dismiss();
            }
        });
    }

    private void dialogTripType()
    {
        final String[] selectedPaymentType = new String[1];

        View view = getLayoutInflater().inflate(R.layout.dialog_order_type_selection,null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        TextView tvSelect = (TextView) view.findViewById(R.id.tvSelect);

        final AlertDialog alert = builder.create();
        alert.show();

        Spinner spinner = (Spinner) view.findViewById(R.id.txtCardType);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                selectedPaymentType[0] = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("One Way Trip");
        categories.add("Round Trip");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter <String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        tvSelect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                txtTripType.setText(selectedPaymentType[0]);
                alert.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKUP_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(ReserveCarDetailsActivity.this, data);

                LatLng location = place.getLatLng();

                pickupPlaceName = place.getName().toString();
                txtPickUpAddress.setText(pickupPlaceName);

                orderPickupLatiutde = String.valueOf(location.latitude);
                orderPickupLongitude = String.valueOf(location.longitude);

                Log.e("Map Fragment", "Place: " + place.getLatLng());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(ReserveCarDetailsActivity.this, data);
                // TODO: Handle the error.
                Log.e("Map Fragment", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if(requestCode == PLACE_DROPOFF_AUTOCOMPLETE_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(ReserveCarDetailsActivity.this, data);

                LatLng location = place.getLatLng();

                dropoffPlaceName = place.getName().toString();
                txtDropOffAddress.setText(dropoffPlaceName);

                orderDropoffLatiutde = String.valueOf(location.latitude);
                orderDropoffLongitude = String.valueOf(location.longitude);

                Log.e("Map Fragment", "Place: " + place.getLatLng());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(ReserveCarDetailsActivity.this, data);
                // TODO: Handle the error.
                Log.e("Map Fragment", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private void orderCar()
    {
        if(true)
        {
            progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",ReserveCarDetailsActivity.this);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            WebServices service = retrofit.create(WebServices.class);

            HashMap<String, String> body = new HashMap<>();

            body.put("userId",(String)CommonObjects.getSharedPreferences(ReserveCarDetailsActivity.this, "userId",""));
            body.put("orderDate",datePick);
            body.put("orderLocation", CommonObjects.getPickupAddress());
            body.put("orderLatitude",String.valueOf(CommonObjects.getPickupLatitude()));
            body.put("orderLongitude",String.valueOf(CommonObjects.getPickupLongitude()));
            body.put("orderInfrontOf","");
            body.put("orderCarName", String.valueOf(CommonObjects.getSharedPreferences(ReserveCarDetailsActivity.this, "SelectedCarName", "")));
            body.put("orderCarkm", String.valueOf(CommonObjects.getSharedPreferences(ReserveCarDetailsActivity.this, "SelectedCarPrice", "")));
            body.put("orderCarCatID", String.valueOf(CommonObjects.getSharedPreferences(ReserveCarDetailsActivity.this, "SelectedCarID", "")));
            body.put("orderTime",timePick);

            Log.e("Order Body", body.toString());

            service.orderCar(body).enqueue(new Callback<Object>()
            {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {

                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());

                    if (!json.equals("") || json != null || !json.isEmpty())
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            Log.e("Order Confirm", success + " - " + message);
                            Log.e("Order Confirm", json.toString());

                            if(success.equals("1"))
                            {
                                progressDialog.dismiss();
                                gotoThankYouActivity();
                                //Toast.makeText(OrderCarDetailsActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(ReserveCarDetailsActivity.this, message, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    } else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(ReserveCarDetailsActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {

                }
            });
        }
        else
        {

        }
    }

    private void gotoThankYouActivity()
    {
        Intent intent = new Intent(this, ReserverThankYouActivity.class);
        intent.putExtra("date",datePick);
        intent.putExtra("time",timePick);
        intent.putExtra("location",CommonObjects.getPickupAddress());
        startActivity(intent);
        finish();
    }
}

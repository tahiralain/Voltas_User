package net.voltasinc.voltasuser.model;

import java.util.ArrayList;

public class OrderCarHistoryResponse {
	private ArrayList<OrderCarHistoryDataItems> data;
	private String success;
	private String message;

	public void setData(ArrayList<OrderCarHistoryDataItems> data){
		this.data = data;
	}

	public ArrayList<OrderCarHistoryDataItems> getData(){
		return data;
	}

	public void setSuccess(String success){
		this.success = success;
	}

	public String getSuccess(){
		return success;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}
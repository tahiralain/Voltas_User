package net.voltasinc.voltasuser.model;

public class OrderCarHistoryDataItems {
	private String orderLatitude;
	private String orderInfrontOf;
	private String orderId;
	private String totalPrice;
	private String orderCarKm;
	private String orderTime;
	private String driverId;
	private String orderComplete;
	private String orderCarName;
	private String orderApprove;
	private String paid;
	private String orderLongitude;
	private String orderLocation;
	private String orderDate;

	public void setOrderLatitude(String orderLatitude){
		this.orderLatitude = orderLatitude;
	}

	public String getOrderLatitude(){
		return orderLatitude;
	}

	public void setOrderInfrontOf(String orderInfrontOf){
		this.orderInfrontOf = orderInfrontOf;
	}

	public String getOrderInfrontOf(){
		return orderInfrontOf;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setTotalPrice(String totalPrice){
		this.totalPrice = totalPrice;
	}

	public String getTotalPrice(){
		return totalPrice;
	}

	public void setOrderCarKm(String orderCarKm){
		this.orderCarKm = orderCarKm;
	}

	public String getOrderCarKm(){
		return orderCarKm;
	}

	public void setOrderTime(String orderTime){
		this.orderTime = orderTime;
	}

	public String getOrderTime(){
		return orderTime;
	}

	public void setDriverId(String driverId){
		this.driverId = driverId;
	}

	public String getDriverId(){
		return driverId;
	}

	public void setOrderComplete(String orderComplete){
		this.orderComplete = orderComplete;
	}

	public String getOrderComplete(){
		return orderComplete;
	}

	public void setOrderCarName(String orderCarName){
		this.orderCarName = orderCarName;
	}

	public String getOrderCarName(){
		return orderCarName;
	}

	public void setOrderApprove(String orderApprove){
		this.orderApprove = orderApprove;
	}

	public String getOrderApprove(){
		return orderApprove;
	}

	public void setPaid(String paid){
		this.paid = paid;
	}

	public String getPaid(){
		return paid;
	}

	public void setOrderLongitude(String orderLongitude){
		this.orderLongitude = orderLongitude;
	}

	public String getOrderLongitude(){
		return orderLongitude;
	}

	public void setOrderLocation(String orderLocation){
		this.orderLocation = orderLocation;
	}

	public String getOrderLocation(){
		return orderLocation;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}
}

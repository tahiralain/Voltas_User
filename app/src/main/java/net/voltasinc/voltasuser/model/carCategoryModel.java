package net.voltasinc.voltasuser.model;

/**
 * Created by voltas on 8/7/17.
 */

public class carCategoryModel {

    String CategoryID;
    String CategoryName;
    String CategoryBaseFare;
    String CategoryPerKMFare;

    public void setCategoryPerMinFare(String categoryPerMinFare) {
        CategoryPerMinFare = categoryPerMinFare;
    }

    public void setCategoryPerKMFare(String categoryPerKMFare) {
        CategoryPerKMFare = categoryPerKMFare;
    }

    public void setCategoryBaseFare(String categoryBaseFare) {
        CategoryBaseFare = categoryBaseFare;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    String CategoryPerMinFare;

    public String getCategoryID() {
        return CategoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public String getCategoryBaseFare() {
        return CategoryBaseFare;
    }

    public String getCategoryPerKMFare() {
        return CategoryPerKMFare;
    }

    public String getCategoryPerMinFare() {
        return CategoryPerMinFare;
    }
}

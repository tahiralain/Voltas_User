package net.voltasinc.voltasuser.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.voltasinc.voltasuser.R;


/**
 * Created by hp on 3/15/2017.
 */

public class AboutUsFragment extends Fragment
{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return  inflater.inflate(R.layout.fragment_about_us,container,false);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //((AfterLoginActivity)getActivity()).changeActionBarText("About Us");
    }
}

package net.voltasinc.voltasuser.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.voltasinc.voltasuser.R;

/**
 * Created by hp on 3/15/2017.
 */

public class TermsAndConditionsFragment extends Fragment
{
    WebView mWebView;
    private ProgressDialog progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_terms_and_conditions,container,false);

        //((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        mWebView = (WebView) v.findViewById(R.id.webview1);
        mWebView.loadUrl("http://voltasinc.net/legal.php");

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        progressBar = ProgressDialog.show(getActivity(), "Terms & Conditions", "Loading...");

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("Terms", "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i("Terms", "Finished loading URL: " +url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                  return;
                    }
                });

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //((AfterLoginActivity)getActivity()).changeActionBarText("Terms And Conditions");
    }
}

package net.voltasinc.voltasuser.fragment;


import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;


/**
 * Created by hp on 3/14/2017.
 */


public class UserProfileFragment extends Fragment
{
    public TextView UserName;
    public TextView UserEmail;
    public TextView UserMobile;
    public ImageView userImage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_user_profile,container,false);

        UserName = (TextView) view.findViewById(R.id.UserName);
        UserEmail = (TextView) view.findViewById(R.id.UserEmail);
        UserMobile = (TextView) view.findViewById(R.id.UserPhone);
        userImage = (ImageView) view.findViewById(R.id.ivLogo);

        UserName.setText((String) CommonObjects.getSharedPreferences(getContext(), "userFullName", ""));
        UserEmail.setText((String) CommonObjects.getSharedPreferences(getContext(), "userEmail", ""));
        UserMobile.setText((String) CommonObjects.getSharedPreferences(getContext(), "userMobile", ""));

        String userImageURL = CommonObjects.getSharedPreferences(getContext(), "userImage", "").toString();

        try {
            userImage.setImageBitmap(BitmapFactory.decodeStream((InputStream) new URL(userImageURL).getContent()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
         menu.clear();
        inflater.inflate(R.menu.user_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //((AfterLoginActivity)getActivity()).changeActionBarText("User Profile");
    }



}

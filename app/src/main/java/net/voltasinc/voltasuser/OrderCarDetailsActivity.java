package net.voltasinc.voltasuser;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderCarDetailsActivity extends AppCompatActivity
{
    @BindView(R.id.txtPickUpAddress)
    EditText txtPickUpAddress;
    @BindView(R.id.txtPickUpDate)
    EditText txtPickUpDate;
    @BindView(R.id.txtInfrontOf)
    EditText txtInfrontOf;
    @BindView(R.id.txtCarSelected)
    EditText txtCarSelected;
    @BindView(R.id.txtDescription)
    EditText txtDescription;
    @BindView(R.id.tvOrderNow)
    TextView tvOrderNow;
    @BindView(R.id.tvCardDetails)
    TextView tvCardDetails;

    String selectedCarName="";
    String selectedCarPrice ="";
    String datePick,timePick;
    String address;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        changeActionBarText("Order Details");
        ButterKnife.bind(this);

        txtPickUpAddress.setEnabled(false);
        txtPickUpAddress.setText(CommonObjects.getPickupAddress());

        if(!CommonObjects.getSharedPreferences(this, "SelectedCarName","").toString().equals(""))
        {
            selectedCarName = CommonObjects.getSharedPreferences(this, "SelectedCarName","").toString();
            selectedCarPrice = CommonObjects.getSharedPreferences(this, "SelectedCarPrice","").toString();
            txtCarSelected.setText(selectedCarName+" "+selectedCarPrice);
        }
    }

    @OnClick({R.id.txtPickUpAddress, R.id.txtPickUpDate, R.id.tvOrderNow, R.id.tvCardDetails})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtPickUpAddress:
                Intent i = new Intent(this, MapsActivity.class);
                startActivityForResult(i, 1001);
                break;
            case R.id.txtPickUpDate:
                dateTimeDialog();
                break;
            case R.id.tvOrderNow:
                orderCar();
                break;
            case R.id.tvCardDetails:
                cardDetails();
                break;
        }
    }
    private void dateTimeDialog()
    {
       // Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        date d = new date();
        time t = new time();
        TimePickerDialog tp = new TimePickerDialog(this,t,mHour, mMinute, false);
        tp.show();
        DatePickerDialog dpd = new DatePickerDialog(this,d, mYear, mMonth, mDay);
        dpd.show();
    }

    private void cardDetails() {
        startActivity(new Intent(this, userCardDetails.class));
    }

    class  date implements DatePickerDialog.OnDateSetListener
    {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            datePick= " "+dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 1001)
        {
            if (resultCode == RESULT_OK)
            {
                address= data.getStringExtra("result");
                txtPickUpAddress.setText(address);
            }
        }
    }

    class  time implements TimePickerDialog.OnTimeSetListener
    {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute)
        {
            timePick=hourOfDay + ":" + minute;
            txtPickUpDate.setText(datePick+" "+timePick);
        }
    }

    public void changeActionBarText(String text)
    {
        ActionBar a = getSupportActionBar();
        a.setCustomView(R.layout.layout_item_actionbar);
        a.setDisplayShowCustomEnabled(true);
        TextView tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        tvActionBarTitle.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transparent, menu);
        return true;
    }


    private void orderCar()
    {
        if(true)
        {
            progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",OrderCarDetailsActivity.this);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            WebServices service = retrofit.create(WebServices.class);

            HashMap<String, String> body = new HashMap<>();

            body.put("userId",(String)CommonObjects.getSharedPreferences(OrderCarDetailsActivity.this, "userId",""));
            body.put("orderDate",datePick);

            body.put("orderLocation", CommonObjects.getPickupAddress());
            body.put("orderLatitude",String.valueOf(CommonObjects.getPickupLatitude()));
            body.put("orderLongitude",String.valueOf(CommonObjects.getPickupLongitude()));
            body.put("orderInfrontOf",txtInfrontOf.getText().toString());
            body.put("orderCarName",selectedCarName);
            body.put("orderCarkm",selectedCarPrice);
            body.put("orderCarCatID", String.valueOf(CommonObjects.getSharedPreferences(OrderCarDetailsActivity.this, "SelectedCarID", "")));
            body.put("orderTime",timePick);

            service.orderCar(body).enqueue(new Callback<Object>()
            {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());

                    if (!json.equals("") || json != null || !json.isEmpty())
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            Log.e("Order Confirm", success + " - " + message);
                            Log.e("Order Confirm", json.toString());

                            if(success.equals("1"))
                            {

                                progressDialog.dismiss();
                                gotoThankYouActivity();
                                //Toast.makeText(OrderCarDetailsActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(OrderCarDetailsActivity.this, message, Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    } else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(OrderCarDetailsActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {

                }
            });
        }
        else
        {

        }
    }

    private void gotoThankYouActivity()
    {
        Intent intent = new Intent(this, ThankYouActivity.class);
        intent.putExtra("date",datePick);
        intent.putExtra("time",timePick);
        intent.putExtra("location",CommonObjects.getPickupAddress());
        startActivity(intent);
        finish();
    }
}

package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.CustomAlertDialog;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginUserActivity extends AppCompatActivity
{

    // Initializing required views
    // -->start
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.tvSignIn)
    TextView tvSignIn;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.rememberPassword)
    CheckBox rememberPassword;
    // <-- End of Initializing views

    private ProgressDialog progressDialog;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Hide the status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);
        ButterKnife.bind(this);

        // Hiding action bar
        getSupportActionBar().hide();

        pref = this.getSharedPreferences("RememberPassword", Context.MODE_PRIVATE);
        edit = pref.edit();

        if( pref.getString("RememberPassword", "").equalsIgnoreCase("true") ) {

            Log.e("Remember Pass", "TRUE");

            Log.e("Remember Email", pref.getString("UserEmail", ""));

            Log.e("Remember Password", pref.getString("UserPassword", ""));

            txtEmail.setText(pref.getString("UserEmail", ""));
            txtPassword.setText(pref.getString("UserPassword", ""));
        }

    }// <-- End Of onCreate() Method


    @OnClick({R.id.tvSignIn, R.id.tvRegister, R.id.tvForgotPassword})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.tvSignIn:
            {
                if(CommonObjects.isOnline(this))
                {
                    Login();
                }
                else
                {
                      new CustomAlertDialog(this,"Alert","Please Connect Your Internet");
                }


                break;
            }
            case R.id.tvRegister:
            {
                // Going to Register User Activity
                startActivity(new Intent(LoginUserActivity.this,RegisterUserActivity.class));
                break;
            }
            case R.id.tvForgotPassword:
            {
                startActivity(new Intent(LoginUserActivity.this, forgotPassword.class));
                break;
            }
        }
    }


    private void Login()
    {
        if(checkValidation())
        {
            progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",LoginUserActivity.this);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            WebServices service = retrofit.create(WebServices.class);

            HashMap<String, String> body = new HashMap<>();
           // body.put("name", txtUserName.getText().toString());
            body.put("email", txtEmail.getText().toString());
            body.put("password", txtPassword.getText().toString());
           // body.put("mobile", txtMobileNumber.getText().toString());
            String firebaseId = (String)CommonObjects.getSharedPreferences(getApplicationContext(), "fcmId", "");
            body.put("firebaseId",firebaseId);
            body.put("role", "user");

            Log.e("Login", body.toString());

            service.loginUser(body).enqueue(new Callback<Object>()
            {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());

                    if (!json.equals("") || json != null || !json.isEmpty()) {
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if(success.equals("1"))
                            {

                                if(rememberPassword.isChecked()){
                                    edit.putString("RememberPassword", "true");
                                    edit.commit();
                                    edit.apply();

                                    edit.putString("UserEmail", txtEmail.getText().toString());
                                    edit.commit();
                                    edit.apply();

                                    edit.putString("UserPassword", txtPassword.getText().toString());
                                    edit.commit();
                                    edit.apply();

                                    Log.e("Remember Pass", "Checked");
                                }

                                JSONObject data = jsonObject.getJSONObject("data");

                                Log.e("Login User Data", data.toString());

                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "LoggedIn", true);
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userImage", data.getString("image"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userId", data.getString("userId"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userFullName", data.getString("userFullName"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userEmail", data.getString("userEmail"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userPassword", data.getString("userPassword"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userMobile", data.getString("userMobile"));
                                CommonObjects.saveSharedPreferences(LoginUserActivity.this, "userWallet", data.getString("WalletBalance"));

                                Log.e("Login User Image", CommonObjects.getSharedPreferences(LoginUserActivity.this, "userImage", "").toString());

                                progressDialog.dismiss();
                                gotoAfterLoginActivity();
                                Toast.makeText(LoginUserActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(LoginUserActivity.this, message, Toast.LENGTH_LONG).show();

                            }



                        } catch (JSONException e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();

                        }
                    } else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(LoginUserActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {

                    t.getMessage();
                }
            });
        }
        else
        {

        }
    }

    private void gotoAfterLoginActivity()
    {
        startActivity(new Intent(this,AfterLoginActivity.class));
        finish();
    }


    private boolean checkValidation()
    {
        if(!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches())
        {

            Toast.makeText(this,"Enter Correct Email",Toast.LENGTH_LONG).show();

            return false;
        }

        else if(txtPassword.getText().toString().length() < 6)
        {

            Toast.makeText(this,"Enter Password Minimum Of 6 Letters",Toast.LENGTH_LONG).show();

            return false;
        }

        return true;
    }
}

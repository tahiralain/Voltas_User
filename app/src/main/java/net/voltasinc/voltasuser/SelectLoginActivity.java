package net.voltasinc.voltasuser;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static net.voltasinc.voltasuser.fragment.MapFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class SelectLoginActivity extends AppCompatActivity {

    @BindView(R.id.tvSignIn)
    TextView tvSignIn;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    public  final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Hide the status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_login);
        // Hiding action bar
        getSupportActionBar().hide();

        ButterKnife.bind(this);
        checkLocationPermission();

    }

    @OnClick({R.id.tvSignIn, R.id.tvRegister})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.tvSignIn:
                startActivity(new Intent(this,LoginUserActivity.class));
                finish();
                break;
            case R.id.tvRegister:
                startActivity(new Intent(this,RegisterUserActivity.class));
                finish();
                break;
        }
    }

    public void checkLocationPermission()
    {
        try {
            if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION))
                {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    //Prompt the user once explanation has been shown
                    ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
                else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }

            }
        }
        catch(Exception e)
        {

        }
    }

}

package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.adapters.OrderCarListAdapter;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;
import net.voltasinc.voltasuser.model.carCategoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderCarListActivity extends AppCompatActivity {


    ArrayList carPricesArrayList;
    @BindView(R.id.rvOrderCarList)
    RecyclerView rvOrderCarList;
    private ProgressDialog progressDialog;

    List<carCategoryModel> CarCategoryDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_car_list);
        ButterKnife.bind(this);
        changeActionBarText("Select You Car");
        CarCategoryDataList = new ArrayList<>();

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvOrderCarList.setLayoutManager(horizontalLayoutManager);

        getAllCategories();
    }

    private void getAllCategories()
    {

        progressDialog = new LoaderDialog().showProgressDialog("Please Wait...", OrderCarListActivity.this);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        HashMap<String, String> body = new HashMap<>();
        body.put("role", "user");


        service.getAllCategories(body).enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                if (!json.equals("") || json != null || !json.isEmpty()) {
                    try {

                        Log.e("Car Categories", json);
                        JSONObject jsonObject = new JSONObject(json);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        Log.e("Car Categories Array", jsonArray.toString());

                        for(int i = 0; i < jsonArray.length(); i++)
                        {
                            carCategoryModel CategoryData = new carCategoryModel();

                            JSONObject jsonObject1 = null;

                            try{

                                jsonObject1 = jsonArray.getJSONObject(i);

                                CategoryData.setCategoryBaseFare(jsonObject1.getString("Base_Fare"));
                                CategoryData.setCategoryID(jsonObject1.getString("cat_id"));
                                CategoryData.setCategoryName(jsonObject1.getString("cat_name"));
                                CategoryData.setCategoryPerKMFare(jsonObject1.getString("Per_Km") + "/km");
                                CategoryData.setCategoryPerMinFare(jsonObject1.getString("Per_Minute"));

                            } catch (JSONException e){
                                e.printStackTrace();
                            }

                            CarCategoryDataList.add(CategoryData);
                        }

                        String[] carPrices = {"1.90/km", "2.65/km", "1.80/km", "1.80/km"};
                        // Converting carPrices in to array list to pass from RecyclerView
                        carPricesArrayList = new ArrayList(Arrays.asList(carPrices));
                        OrderCarListAdapter adapter = new OrderCarListAdapter(CarCategoryDataList, OrderCarListActivity.this);
                        rvOrderCarList.setAdapter(adapter);

                        progressDialog.dismiss();

                    } catch (JSONException e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                } else
                {
                    progressDialog.dismiss();
                    Toast.makeText(OrderCarListActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });

    }


    public void changeActionBarText(String text)
    {
        ActionBar a = getSupportActionBar();
        a.setCustomView(R.layout.layout_item_actionbar);
        a.setDisplayShowCustomEnabled(true);
        TextView tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        tvActionBarTitle.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transparent, menu);
        return true;
    }

}

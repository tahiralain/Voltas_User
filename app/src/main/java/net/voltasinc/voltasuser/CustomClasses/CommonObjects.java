package net.voltasinc.voltasuser.CustomClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.appcompat.BuildConfig;

// This class Deals with shared preferences
public class CommonObjects
{

    public static String pickupAddress;
    public static double pickupLatitude;

    public static void setPickupLongitude(double pickupLongitude) {
        CommonObjects.pickupLongitude = pickupLongitude;
    }

    public static void setPickupLatitude(double pickupLatitude) {
        CommonObjects.pickupLatitude = pickupLatitude;
    }

    public static void setPickupAddress(String pickupAddress) {
        CommonObjects.pickupAddress = pickupAddress;
    }

    public static double pickupLongitude;

    public static String getPickupAddress() {
        return pickupAddress;
    }

    public static double getPickupLatitude() {
        return pickupLatitude;
    }

    public static double getPickupLongitude() {
        return pickupLongitude;
    }

    public CommonObjects(Context context)
    {
        // this.context=context;
        // init();
    }
    public static String getDeviceId(Context context)
    {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getPlatform()
    {
        return "a";
    }

    public static int getPlatformVersion()
    {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String getAppVersion()
    {
        return BuildConfig.VERSION_NAME;
    }

    public static Object getSharedPreferences(Context context, String key, Object value) {
        SharedPreferences pref;
        pref = context.getSharedPreferences("Feeling Blessed", Context.MODE_PRIVATE);
        if (value instanceof Boolean)
            return pref.getBoolean(key, (boolean) value);
        else if (value instanceof Integer)
            return pref.getInt(key, (int) value);
        else if (value instanceof String)
            return pref.getString(key, (String) value);

        else
            return null;
        //JSONRequest.sAuthCode=pref.getString("authCode", "");
        //JSONRequest.sMemberId=pref.getInt("MemberId",-1);
    }

    public static void clearSharePreferences(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences("Feeling Blessed", Context.MODE_PRIVATE);
        pref.edit().clear().commit();
        //SharedPreferences.Editor edit = pref.edit();
        //edit.clear();
        //edit.commit();
    }

    public static void saveSharedPreferences(Context context, String key, Object value)
    {
        SharedPreferences pref = context.getSharedPreferences("Feeling Blessed", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();

        if (value instanceof Boolean)
            edit.putBoolean(key, (boolean) value);
        else if (value instanceof Integer)
            edit.putInt(key, (int) value);
        else if (value instanceof String)
            edit.putString(key, (String) value);

        edit.commit();
        edit.apply();
    }


       public static boolean hasSharedPreferences(Context context, String key)
    {
        SharedPreferences pref = context.getSharedPreferences("Feeling Blessed", Context.MODE_PRIVATE);
//        SharedPreferences.Editor edit = pref.edit();
//        if (value instanceof Boolean)
//            edit.putBoolean(key, (boolean) value);
//        else if (value instanceof Integer)
//            edit.putInt(key, (int) value);
//        else if (value instanceof String)
//            edit.putString(key, (String) value);

        return pref.contains(key);
    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected())
            return true;

        return false;
    }

}

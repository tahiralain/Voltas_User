package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class forgotPassword extends AppCompatActivity {

    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.tvSignIn)
    TextView tvSignIn;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);

        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",forgotPassword.this);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constant.baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                WebServices service = retrofit.create(WebServices.class);

                HashMap<String, String> body = new HashMap<>();
                body.put("email", txtEmail.getText().toString());

                Log.e("Email", body.toString());

                service.forgotPassword(body).enqueue(new Callback<Object>()
                {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Gson gson = new Gson();
                        String json = gson.toJson(response.body());

                        Log.e("response", json);

                        if (!json.equals("") || json != null || !json.isEmpty()) {
                            try {
                                JSONObject jsonObject = new JSONObject(json);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");

                                if(success.equals("1"))
                                {
                                    JSONObject data = jsonObject.getJSONObject("data");

                                    progressDialog.dismiss();
                                    Toast.makeText(forgotPassword.this, message, Toast.LENGTH_LONG).show();
                                    forgotPassword.super.onBackPressed();
                                }
                                else
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(forgotPassword.this, message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e)
                            {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        } else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(forgotPassword.this, "Internet Error", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                    }
                });
            }
        });
    }
}

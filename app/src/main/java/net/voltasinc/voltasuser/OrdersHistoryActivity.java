package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.adapters.OrderHistoryListAdapter;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;
import net.voltasinc.voltasuser.model.OrderCarHistoryDataItems;
import net.voltasinc.voltasuser.model.OrderCarHistoryResponse;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrdersHistoryActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    @BindView(R.id.rvOrders)
    RecyclerView rvOrders;
    @BindView(R.id.tvEmptyView)
    TextView tvEmptyView;
    @BindView(R.id.tvAllOrders)
    TextView tvAllOrders;
    @BindView(R.id.tvApproved)
    TextView tvApproved;
    @BindView(R.id.tvCompleted)
    TextView tvCompleted;
    static String approve = "0",  completed = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);
        ButterKnife.bind(this);

        changeActionBarText("Order Car History");
        getOrders("0", "0", (String) CommonObjects.getSharedPreferences(OrdersHistoryActivity.this, "userId", "0"));
    }

    private void getOrders(String approve, String complete, String userId)
    {
        progressDialog = new LoaderDialog().showProgressDialog("Please Wait...", OrdersHistoryActivity.this);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        HashMap<String, String> body = new HashMap<>();

        body.put("approve", approve);
        body.put("complete", complete);
        body.put("userId", userId);

        service.orderCarHistory(body).enqueue(new Callback<OrderCarHistoryResponse>() {
            @Override
            public void onResponse(Call<OrderCarHistoryResponse> call, Response<OrderCarHistoryResponse> response) {

                OrderCarHistoryResponse orderHistoryResponse = response.body();

                if (orderHistoryResponse != null) {
                    try {

                        String success = orderHistoryResponse.getSuccess();
                        String message = orderHistoryResponse.getMessage();

                        if (success.equals("1"))
                        {
                            ArrayList<OrderCarHistoryDataItems> values = orderHistoryResponse.getData();

                            rvOrders.setVisibility(View.VISIBLE);
                            setRecyclerView(values);
                            tvEmptyView.setVisibility(View.GONE);
                            progressDialog.dismiss();


                        } else if (success.equals("0"))
                        {
                            progressDialog.dismiss();

                            rvOrders.setVisibility(View.GONE);
                            tvEmptyView.setVisibility(View.VISIBLE);
                            Toast.makeText(OrdersHistoryActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(OrdersHistoryActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<OrderCarHistoryResponse> call, Throwable t) {

            }
        });
    }


    @OnClick({R.id.tvAllOrders, R.id.tvApproved, R.id.tvCompleted})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvAllOrders:
                getOrders("0", "0", (String) CommonObjects.getSharedPreferences(OrdersHistoryActivity.this, "userId", "0"));
                changeActionBarText("All Orders");
                break;
            case R.id.tvApproved:
                getOrders("1", "0", (String) CommonObjects.getSharedPreferences(OrdersHistoryActivity.this, "userId", "0"));
                changeActionBarText("Approved Orders");
                break;
            case R.id.tvCompleted:
                getOrders("1", "1", (String) CommonObjects.getSharedPreferences(OrdersHistoryActivity.this, "userId", "0"));
                changeActionBarText("Completed Orders");
                break;
        }
    }

    private void setRecyclerView(ArrayList<OrderCarHistoryDataItems> values)
    {
        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvOrders.setLayoutManager(horizontalLayoutManager);
        OrderHistoryListAdapter adapter = new OrderHistoryListAdapter(values, this);
        rvOrders.setAdapter(adapter);
    }


    public void changeActionBarText(String text)
    {
        ActionBar a = getSupportActionBar();
        a.setCustomView(R.layout.layout_item_actionbar);
        a.setDisplayShowCustomEnabled(true);
        TextView tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        tvActionBarTitle.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transparent, menu);
        return true;
    }
}

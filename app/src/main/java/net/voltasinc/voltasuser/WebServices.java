package net.voltasinc.voltasuser;

import net.voltasinc.voltasuser.model.OrderCarHistoryResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by free on 2/9/2017.
 */

public interface WebServices
{
    @POST("voltasRegisterUser.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> registerUser(@Body HashMap<String, String> body);

    @POST("voltasLogin.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> loginUser(@Body HashMap<String, String> body);

    @POST("voltasOrderCar.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> orderCar(@Body HashMap<String, String> body);

    @POST("card_update.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> cardUpdate(@Body HashMap<String, String> body);

    @POST("voltasUserOrderCarHistory.php")
    @Headers("Content-Type:application/json")
    public  Call<OrderCarHistoryResponse> orderCarHistory(@Body HashMap<String, String> body);

    @POST("voltasForgotPassword.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> forgotPassword(@Body HashMap<String, String> body);

    @POST("userUpdateFCMID.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> sendTokenToServer(@Body HashMap<String, String> body);

    @POST("voltasUserPaymentPaid.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> userPaymentPaid(@Body HashMap<String, String> body);

    @POST("updateUserWalletBalance.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> updateUserWalletBalance(@Body HashMap<String, String> body);

    @POST("viewUserWalletBalance.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> viewUserWalletBalance(@Body HashMap<String, String> body);

    @POST("voltasGetAllCategories.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> getAllCategories(@Body HashMap<String, String> body);

    @POST("getDriverCurrentLocation.php")
    @Headers("Content-Type:application/json")
    public  Call<Object> getDriverCurrentLocation(@Body HashMap<String, String> body);

}

package net.voltasinc.voltasuser;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.LogoutConformationDialog;
import net.voltasinc.voltasuser.fragment.AboutUsFragment;
import net.voltasinc.voltasuser.fragment.MapFragment;
import net.voltasinc.voltasuser.fragment.TermsAndConditionsFragment;
import net.voltasinc.voltasuser.fragment.UserProfileFragment;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AfterLoginActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentTransaction fragmentTransaction;

    TextView tvUserNameNav, tvUserEmailNav;
    RelativeLayout rvContentAfterLogin;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mergeViews();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        String fcmToken = FirebaseInstanceId.getInstance().getToken();

        CommonObjects.saveSharedPreferences(getApplicationContext(), "fcmId", fcmToken);

        Log.e("FCM ID", fcmToken);

        if ((Boolean) CommonObjects.getSharedPreferences(AfterLoginActivity.this, "LoggedIn", false)) {
            tvUserNameNav.setText((String) CommonObjects.getSharedPreferences(AfterLoginActivity.this, "userFullName", ""));
            tvUserEmailNav.setText((String) CommonObjects.getSharedPreferences(AfterLoginActivity.this, "userEmail", ""));
        }

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.liContent, new MapFragment());
        fragmentTransaction.commit();

        sendFCMTokenToServer();
    }

    private void sendFCMTokenToServer()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        HashMap<String, String> body = new HashMap<>();
        String firebaseId = (String)CommonObjects.getSharedPreferences(getApplicationContext(), "fcmId", "");
        body.put("regId",firebaseId);
        body.put("driverId", (String)CommonObjects.getSharedPreferences(getApplicationContext(), "userId", ""));

        Log.e("Device ID Update", body.toString());

        service.sendTokenToServer(body).enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
            }
        });
    }

    private void mergeViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View v = navigationView.getHeaderView(0);
        tvUserNameNav = (TextView) v.findViewById(R.id.tvUserNameNav);
        tvUserEmailNav = (TextView) v.findViewById(R.id.tvUserEmailNav);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Toast.makeText(this, "Please Use Navigation Bar on left side to go back.", Toast.LENGTH_LONG).show();

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START))
//        {
//            drawer.closeDrawer(GravityCompat.START);
//        }
//        else
//        {
//
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Home) {
            changeScreen(1);
        }
        else if (id == R.id.nav_Profile) {
            changeScreen(2);
        } else if (id == R.id.nav_wallet) {
            changeScreen(8);
        } else if (id == R.id.nav_OrderHistory) {
            changeScreen(3);
        } else if (id == R.id.nav_TellAFriend) {
            showChooserDialog();
        } else if (id == R.id.nav_AboutUs) {
            changeScreen(6);
        } else if (id == R.id.nav_TermsAndConditions) {
            changeScreen(7);
        } else if (id == R.id.nav_Logout) {
            new LogoutConformationDialog(this);
        } else if (id == R.id.nav_RateTheApp) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=net.voltasinc.voltasuser"));
            startActivity(intent);
        } else if (id == R.id.nav_ContactUs) {
            showContactDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showContactDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.contact_us);

        Button btn_call = (Button) dialog.findViewById(R.id.phone);
        Button btn_email = (Button) dialog.findViewById(R.id.email);

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = "tel:0013473421060";
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(uri));
                if (ActivityCompat.checkSelfPermission(AfterLoginActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /* Create the Intent */
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

/* Fill it with Data */
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@voltasinc.net"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Voltas Contact Info");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

/* Send it off to the Activity-Chooser */
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });

        dialog.show();
    }

    // Choose App to invite Friends
    public void showChooserDialog()
    {
        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Click The Link Below to download the app: \n https://play.google.com/store/apps/details?id=net.voltasinc.voltasuser");
        emailIntent.setType("text/plain");
        Intent openInChooser = Intent.createChooser(emailIntent,"Choose Your App To Invite");
        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, emailIntent);
        startActivity(openInChooser);
    }
    // End Of Above Method

    private void changeScreen(int count)
    {
        switch (count)
        {
            case 1:
            {
                startActivity(new Intent(this,AfterLoginActivity.class));
                this.finish();
                break;
            }
            case 2:
            {
                switchFragmentWithBackStack(new UserProfileFragment());
                break;
            }
            case 3:
            {
                startActivity(new Intent(this,OrdersHistoryActivity.class));
                break;
            }
            case 4:
            {

                break;
            }
            case 5:
            {

            }
            case 6:
            {
                switchFragmentWithBackStack(new AboutUsFragment());
                break;

            }
            case 7:
            {
                switchFragmentWithBackStack(new TermsAndConditionsFragment());
                break;
            }
            case 8:
            {
                startActivity(new Intent(this,UserWallet.class));
                break;
            }
        }
    }

    private void switchFragmentWithBackStack(Fragment fragment)
    {
//        fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.liContent,fragment);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.liContent, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void changeActionBarText(String text)
    {
        ActionBar a = getSupportActionBar();
        a.setCustomView(R.layout.layout_item_actionbar);
        a.setDisplayShowCustomEnabled(true);
        TextView tvActionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        tvActionBarTitle.setText(text);
    }
}

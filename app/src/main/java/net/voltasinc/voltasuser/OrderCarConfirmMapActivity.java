package net.voltasinc.voltasuser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.CustomClasses.paypalConfig;
import net.voltasinc.voltasuser.adapters.OrderHistoryListAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderCarConfirmMapActivity extends FragmentActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    TextView tvTime, tvDate, tvCompleted, tvHomeAddress, tvCancel, tvCustomerName, tvProductTitle, tvConfirm, tvOrderDescription, tvCustomerMobile;
    String[] data;
    TextView tvStartDrive;
    Button btnPayment, trackDriver;
    static String orderCar;
    ProgressDialog progressDialog;
    String currentOrderDriverID;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;

    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(paypalConfig.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_car_confirm_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        Intent getDriverIntent = getIntent();
        currentOrderDriverID = getDriverIntent.getStringExtra("driverID");

        btnPayment = (Button) findViewById(R.id.makePayment);
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(OrderHistoryListAdapter.orderHistoryData.getTotalPrice().equalsIgnoreCase("0") || OrderHistoryListAdapter.orderHistoryData.getTotalPrice().equalsIgnoreCase("")) {
                    Toast.makeText(OrderCarConfirmMapActivity.this, "Amount Due Is Invalid!!!", Toast.LENGTH_LONG).show();
                } else {
                    getPayment();
                }
            }
        });

        trackDriver = (Button) findViewById(R.id.trackDriver);
        trackDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderCarConfirmMapActivity.this, TrackDriver.class);
                intent.putExtra("DriverID", currentOrderDriverID);
                startActivity(intent);
            }
        });

        tvStartDrive = (TextView) findViewById(R.id.tvStartDrive);
        tvCompleted = (TextView) findViewById(R.id.tvCompleted);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvConfirm = (TextView) findViewById(R.id.tvConfirm);
        tvTime = (TextView) findViewById(R.id.tvTime);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvHomeAddress = (TextView) findViewById(R.id.tvHomeAddress);
        tvProductTitle = (TextView) findViewById(R.id.tvProductTitle);
        tvOrderDescription = (TextView) findViewById(R.id.tvOrderDescription);

        tvTime.setText(OrderHistoryListAdapter.orderHistoryData.getOrderTime());
        tvDate.setText(OrderHistoryListAdapter.orderHistoryData.getOrderDate());
        tvHomeAddress.setText(OrderHistoryListAdapter.orderHistoryData.getOrderLocation());
        tvProductTitle.setText(OrderHistoryListAdapter.orderHistoryData.getOrderInfrontOf());
        tvOrderDescription.setText(OrderHistoryListAdapter.orderHistoryData.getOrderCarName()+" "+OrderHistoryListAdapter.orderHistoryData.getOrderCarKm());

        Log.e("Price", "Price: " + OrderHistoryListAdapter.orderHistoryData.getTotalPrice());
    }

    private void getPayment() {
        //Creating a paypalpayment

        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(OrderHistoryListAdapter.orderHistoryData.getTotalPrice())), "USD", "Trip Fare Payment",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    public void gotoTracker(View v)
    {

    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        LatLng l = new LatLng(Double.parseDouble(OrderHistoryListAdapter.orderHistoryData.getOrderLatitude()), Double.parseDouble(OrderHistoryListAdapter.orderHistoryData.getOrderLongitude()));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(l.latitude, l.longitude));

        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        mMap.addMarker(marker);

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(l, 13);
        mMap.animateCamera(update);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        //Starting a new activity for the payment details and also putting the payment details with intent

                        JSONObject jsonDetails = new JSONObject(paymentDetails);
                        JSONObject jsonDetails2 = jsonDetails.getJSONObject("response");

                        String paymentID = jsonDetails2.getString("id");
                        String status = jsonDetails2.getString("state");
                        String amount = "$" + OrderHistoryListAdapter.orderHistoryData.getTotalPrice();

                        String message = "Your Payment Of " + amount + " Has Been " + status + ". Your Payment ID is: " + paymentID;

                        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Congratulations!")
                                .setContentText(message)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        endPaymentTransaction();
                                    }
                                })
                                .show();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    protected void endPaymentTransaction()
    {
        sendPaymentNotification();
        startActivity(new Intent(OrderCarConfirmMapActivity.this, AfterLoginActivity.class));
    }

    protected void sendPaymentNotification()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);

        final HashMap<String, String> body = new HashMap<>();

        body.put("orderId", OrderHistoryListAdapter.orderHistoryData.getOrderId());
        body.put("paymentAmount", OrderHistoryListAdapter.orderHistoryData.getTotalPrice());

        Log.e("Ride Noti", body.toString());

        service.userPaymentPaid(body).enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                Log.e("Ride Noti", body.toString() + " Response:" + json);
            }

            @Override
            public void onFailure(Call<Object > call, Throwable t) {

            }
        });
    }


    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }


    public void confirm()
    {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}

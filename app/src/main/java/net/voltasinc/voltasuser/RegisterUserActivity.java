package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.CustomAlertDialog;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterUserActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener  {

    // initializing all required views
    // --> Start
    @BindView(R.id.txtUserName)
    EditText txtUserName;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.txtReTypePassword)
    EditText txtReTypePassword;
    @BindView(R.id.txtMobileNumber)
    EditText txtMobileNumber;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.google_sign_in_button)
    SignInButton googleSignInButton;
    // <-- End of Initializing required views


    private static final String TAG = RegisterUserActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 1001;

    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog progressDialog;

    private CallbackManager callbackManager;

    private LoginButton loginButton;

    String Username;

    // -->Start OnCreate() method
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (response.getError() != null) {
// handle error
                                } else {
                                    String email = me.optString("email");
                                    System.out.println(me);
                                    Log.e("OnSuccess", me.toString());

                                    Username = me.optString("name");

                                    txtUserName.setText(Username);
                                    txtEmail.setText(email);

// Intent i = new Intent(this, SignUpActivity.class);
                                    //startActivity(new Intent(LoginActivity.this, SignUpActivity.class).putExtra("json", me.toString()));

                                    new SweetAlertDialog(RegisterUserActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Facebook Login!")
                                            .setContentText("Please Fill the remaining fields to successfully register yourself!")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismiss();
                                                }
                                            })
                                            .show();

                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,birthday,email,picture,age_range");
                request.setParameters(parameters);
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

                Log.e("Error!!!", e.getMessage());

            }
        });

        setTextWatcher();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("574200852782-idaev6k110d05bbdnpnfsihnfo2uk7du.apps.googleusercontent.com")
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        googleSignInButton.setSize(SignInButton.SIZE_STANDARD);
        googleSignInButton.setScopes(gso.getScopeArray());

        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }// <-- End OnCreate() method


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "Activity Result");

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Log.e(TAG, "Request Code Success");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            Log.e("Error", "" + requestCode);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e(TAG, "handleSignInResult:" + result.isSuccess());

        Log.e(TAG, "handleSignInResult:" + result.getStatus());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            txtUserName.setText(acct.getDisplayName());
            txtEmail.setText(acct.getEmail());

            new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Google+ Login!")
                    .setContentText("Please Fill the remaining fields to successfully register yourself!")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .show();

        } else {

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    // --> Start applying validation to textfields
    private boolean checkValidation()
    {
        if(!Pattern.compile("[a-zA-Z ]{3,}").matcher(txtUserName.getText().toString().trim()).matches())
        {
            Toast.makeText(this,"Enter Correct Name",Toast.LENGTH_LONG).show();
            txtUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches())
        {

            Toast.makeText(this,"Enter Correct Email",Toast.LENGTH_LONG).show();
            txtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
            return false;
        }
        else if(!Patterns.PHONE.matcher(txtMobileNumber.getText().toString()).matches())
        {
            Toast.makeText(this,"Enter Correct Mobile Number",Toast.LENGTH_LONG).show();
            txtMobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
            return false;
        }
        else if(txtPassword.getText().toString().trim().length() < 6)
        {
            new CustomAlertDialog(this,"Alert","Enter Password Minimum Of 6 Letters");
           // Toast.makeText(this,"Enter Password Minimum Of 6 Letters",Toast.LENGTH_LONG).show();
            txtPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
            return false;
        }
        else if(txtReTypePassword.getText().toString().trim().length() < 6)
        {
            new CustomAlertDialog(this,"Alert","Enter Password Minimum Of 6 Letters");
            //Toast.makeText(this,"Enter Password Minimum Of 6 Letters",Toast.LENGTH_LONG).show();
            txtReTypePassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
            return false;
        }
        else if(!txtPassword.getText().toString().equals(txtReTypePassword.getText().toString()))
        {
            Toast.makeText(this,"Both Passwords Does Not Match",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!CommonObjects.isOnline(this))
        {
            Toast.makeText(this, "Connect Your Internet", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    } // <-- End of checkValidation() method

    // --> Start Applying text watcher on textfields
    private void setTextWatcher()
    {

        txtMobileNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s)
            {
                if(txtMobileNumber.getText().length() == 0)
                {
                    txtMobileNumber.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }

                if(Patterns.PHONE.matcher(s.toString()).matches())
                {
                    txtMobileNumber.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    txtMobileNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0,R.drawable.ic_tick, 0);
                }
                else
                {
                    txtMobileNumber.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }


            }
        });

        txtUserName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s)
            {
                if(txtUserName.getText().length() == 0)
                {
                    txtUserName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                else if(Pattern.compile("[a-zA-Z ]{3,}").matcher(txtUserName.getText().toString().trim()).matches())
                {

                    txtUserName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    txtUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);

                }
                else
                {
                    txtUserName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        });
        txtEmail.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void afterTextChanged(Editable s)
            {
                if(txtEmail.getText().length() == 0)
                {
                    txtEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                else if(Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches())
                {
                    txtEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    txtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);
                }
                else
                {
                    txtEmail.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        });

        txtPassword.addTextChangedListener(new TextWatcher()
        {   @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void afterTextChanged(Editable s)
            {
                if(txtPassword.getText().length() == 0)
                {
                    txtPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                else if(txtPassword.getText().toString().length() >= 6)
                {
                    txtPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    txtPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);
                }
                else
                {
                    txtPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        });
        txtReTypePassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s)
            {
                if(txtReTypePassword.getText().length() == 0)
                {
                    txtReTypePassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                else if(txtReTypePassword.getText().toString().length() >= 6)
                {
                    txtReTypePassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    txtReTypePassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick, 0);
                }
                else
                {
                    txtReTypePassword.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        });


    }
    // <--End of setTextWatcher() method


    @OnClick(R.id.tvRegister)
    public void onClick()
    {
        if(CommonObjects.isOnline(this))
        {
            registerUser();
        }
        else
        {
            new CustomAlertDialog(this,"Alert","Please Connect Your Internet");
        }

    }

    private void registerUser()
    {
        String mobileNumber = txtMobileNumber.getText().toString();
        String newMobileNumber = mobileNumber.replaceFirst("^0+(?!$)", "");

        if(checkValidation())
        {
            progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",RegisterUserActivity.this);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            WebServices service = retrofit.create(WebServices.class);

            HashMap<String, String> body = new HashMap<>();
            body.put("name", txtUserName.getText().toString());
            body.put("email", txtEmail.getText().toString());
            body.put("password", txtPassword.getText().toString());
            body.put("mobile", newMobileNumber);
            String firebaseId = (String)CommonObjects.getSharedPreferences(getApplicationContext(), "fcmId", "");
            body.put("firebaseId",firebaseId);


            service.registerUser(body).enqueue(new Callback<Object>()
            {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response)
                {


                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());

                    if (!json.equals("") || json != null || !json.isEmpty())
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");

                            if(success.equals("1"))
                            {
                                JSONObject data = jsonObject.getJSONObject("data");

                                progressDialog.dismiss();
                                CommonObjects.saveSharedPreferences(RegisterUserActivity.this, "LoggedIn", true);
                                CommonObjects.saveSharedPreferences(RegisterUserActivity.this, "userId", data.getString("userId"));
                                CommonObjects.saveSharedPreferences(RegisterUserActivity.this, "userFullName", data.getString("userFullName"));
                                CommonObjects.saveSharedPreferences(RegisterUserActivity.this, "userEmail", data.getString("userEmail"));
                                CommonObjects.saveSharedPreferences(RegisterUserActivity.this, "userMobile", data.getString("userMobile"));

                                gotoAfterLoginActivity();
                                Toast.makeText(RegisterUserActivity.this, message, Toast.LENGTH_LONG).show();
                            }



                        } catch (JSONException e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();

                        }
                    } else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(RegisterUserActivity.this, "Internet Error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t)
                {

                }
            });
        }
        else
        {

        }
    }

    private void gotoAfterLoginActivity()
    {
        startActivity(new Intent(this,AfterLoginActivity.class));
        finish();
    }
}

package net.voltasinc.voltasuser;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.adapters.OrderHistoryListAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TrackDriver extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String currentOrderDriverID;
    public static Timer repeatTask;
    int repeatInterval = 20000;

    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_driver);

        repeatTask = new Timer();

        Intent intent = getIntent();
        currentOrderDriverID = intent.getStringExtra("DriverID");

        SupportMapFragment supportMapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        startTrackingDriver();
    }

    protected void updateMarkerOnMap(double latitude, double longitude)
    {
        LatLng location = new LatLng(latitude, longitude);

        if(marker == null) {
            marker = mMap.addMarker(new MarkerOptions().position(location).
                    icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        }else {
            marker.setPosition(location);
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location, 15);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.animateCamera(cameraUpdate);

//        marker = new MarkerOptions().position(
//                new LatLng(l.latitude, l.longitude));
//
//        marker.icon(BitmapDescriptorFactory
//                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
//
//        // adding marker
//        mMap.addMarker(marker);
//
//        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(l, 13);
//        mMap.animateCamera(update);
    }

    protected void startTrackingDriver() {

        Log.e("Driver Tracking", "Driver Tracking Started...");

        repeatTask.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constant.baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                WebServices service = retrofit.create(WebServices.class);

                HashMap<String, String> body = new HashMap<>();
                body.put("driverID", currentOrderDriverID);

                Log.e("Location Update", body.toString());

                service.getDriverCurrentLocation(body).enqueue(new Callback<Object>()
                {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {

                        Gson gson = new Gson();
                        String json = gson.toJson(response.body());

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(json);

                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            updateMarkerOnMap(Double.parseDouble(jsonObject1.getString("driverCurrentLat")), Double.parseDouble(jsonObject1.getString("driverCurrentLong")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("Loc Update Response", jsonObject.toString());
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {

                        Log.e("Location Update Error", t.getMessage());
                    }
                });
            }
        }, repeatInterval, repeatInterval);

    }
}

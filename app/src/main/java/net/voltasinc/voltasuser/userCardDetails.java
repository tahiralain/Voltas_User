package net.voltasinc.voltasuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.google.gson.Gson;

import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.CustomClasses.Constant;
import net.voltasinc.voltasuser.dialogs.LoaderDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class userCardDetails extends AppCompatActivity {

    public static String selectedCardType;

//    @BindView(R.id.txtExpirationDate)
//    EditText txtExpirationDate;
//    @BindView(R.id.txtCardName)
//    EditText txtCardName;
//    @BindView(R.id.txtCardNumber)
//    EditText txtCardNumber;
//    @BindView(R.id.txtCVCCode)
//    EditText txtCVCCode;
    @BindView(R.id.tvOK)
    TextView tvOK;
    @BindView(R.id.tvScanCard)
    TextView tvScanCard;

    ProgressDialog progressDialog;
    String ExpirationDate;

    int MY_SCAN_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_card_details);
        ButterKnife.bind(this);

        final CardForm cardForm = (CardForm) findViewById(R.id.card_form);
        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .actionLabel("Add")
                .setup(this);

        tvScanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onScanPress(v);
            }
        });

        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new LoaderDialog().showProgressDialog("Please Wait...",userCardDetails.this);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constant.baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                WebServices service = retrofit.create(WebServices.class);

                HashMap<String, String> body = new HashMap<>();

                body.put("userId",(String) CommonObjects.getSharedPreferences(userCardDetails.this, "userId",""));
                body.put("expireDate",cardForm.getExpirationMonth() + "/" + cardForm.getExpirationYear());
                body.put("cardNumber", cardForm.getCardNumber());
                body.put("cvvCode", cardForm.getCvv());

                Log.e("Params", body.toString());

                service.cardUpdate(body).enqueue(new Callback<Object>()
                {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Log.e("Response", response.body().toString());

                        Gson gson = new Gson();
                        String json = gson.toJson(response.body());

                        if (!json.equals("") || json != null || !json.isEmpty())
                        {
                            try {
                                JSONObject jsonObject = new JSONObject(json);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");

                                Log.e("Order Confirm", success + " - " + message);
                                Log.e("Order Confirm", json.toString());

                                if(success.equals("1"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(userCardDetails.this, message, Toast.LENGTH_LONG).show();
                                    userCardDetails.super.onBackPressed();
                                }
                                else
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(userCardDetails.this, message, Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e)
                            {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        } else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(userCardDetails.this, "Internet Error", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Log.e("Error", call.toString());
                    }
                });

            }
        });

    }

    public void onScanPress(View v) {
        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE, true);
        //scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                Log.e("Card Number", scanResult.getFormattedCardNumber());

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);

            Log.e("Scan Result", resultDisplayStr);
        }
        // else handle other activity results
    }
}

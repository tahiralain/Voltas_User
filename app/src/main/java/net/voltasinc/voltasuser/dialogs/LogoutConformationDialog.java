package net.voltasinc.voltasuser.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;


import net.voltasinc.voltasuser.AfterLoginActivity;
import net.voltasinc.voltasuser.CustomClasses.CommonObjects;
import net.voltasinc.voltasuser.SelectLoginActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;



/**
 * Created by Ramal on 10/21/2015.
 */
public class LogoutConformationDialog
{
    AfterLoginActivity mainActivity;
    String logOut = "Log Out";
    public LogoutConformationDialog(Activity activity)
    {
        mainActivity=(AfterLoginActivity) activity;
        showDialog();
    }

    SweetAlertDialog sDialog;
    private void showDialog()
    {
        sDialog  = new SweetAlertDialog(mainActivity, SweetAlertDialog.WARNING_TYPE);

        sDialog.setTitleText("Log out")
            .setContentText("Are you sure you want to Log out ?")
            .setConfirmText("Yes")
            .setCancelText("No")
             .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener()
                {
                    @Override
                    public void onClick(SweetAlertDialog sDialog)
                    {
                        sDialog.cancel();
                    }
                })
            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener()
            {
                @Override
                public void onClick(SweetAlertDialog sDialog)
                {
                    okPress();
                    sDialog
                            .setTitleText("Takecare")
                            .setContentText("Thank You For Using The App")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener()
                            {
                            @Override
                            public void onClick(SweetAlertDialog sDialog)
                            {

                            }
                        })
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                }
            })
            .show();
    }

            public void okPress()
            {



                CommonObjects.clearSharePreferences(mainActivity);

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                      sDialog.cancel();


                      mainActivity.startActivity(new Intent(mainActivity, SelectLoginActivity.class));
                      mainActivity.finish();

                    }
                },1000);
            }


    private void signOut()
    {

    }
}

package net.voltasinc.voltasuser.dialogs;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Irtiza on 3/9/2017.
 */

public class LoaderDialog
{

    public ProgressDialog showProgressDialog(String text, Context context)
    {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(text);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        return progressDialog;
    }

}
